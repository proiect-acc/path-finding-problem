#include "Harta.h"
#include "Graph.h"

using namespace std;

int main()
{
	Harta harta;
	Graph graf;
	int x, y;

	harta.citire_harta("date.txt");

	cout << "Harta din fisier este:" << endl << endl;
	harta.afisare_harta();
	cout << endl;

	if (graf.formare(harta))
	{
		cout << "Harta a fost transformata cu succes intr-un graf reprezentat prin liste de adiacenta." << endl;
	}

	cout << endl;
	cout << "Introduceti coordonatele punctului de start:" << endl;
	cout << "x= "; cin >> x;
	cout << "y= "; cin >> y;
	while (x > harta.getNrLinii() - 1 || y > harta.getNrColoane() - 1 || x < 0 || y < 0 || harta.getLabirint()[x][y] == -1)
	{
		cout << "Coordonate incorecte. Reintroduceti." << endl;
		cout << "x= "; cin >> x;
		cout << "y= "; cin >> y;
	}

	pair<int, int> start = make_pair(x, y);

	cout << "Introduceti coordonatele punctului de sosire:" << endl;
	cout << "x= "; cin >> x;
	cout << "y= "; cin >> y;
	while (x > harta.getNrLinii() - 1 || y > harta.getNrColoane() - 1 || x < 0 || y < 0 || harta.getLabirint()[x][y] == -1)
	{
		cout << "Coordonate incorecte. Reintroduceti." << endl;
		cout << "x= "; cin >> x;
		cout << "y= "; cin >> y;
	}

	pair<int, int> end = make_pair(x, y);

	cout << endl;
	graf.afisare_drum(start, end, harta);

	cout << endl;

	harta.dealoc();

	return 0;
}