#include "Harta.h"

void Harta::alocare(int nr_linii, int nr_coloane)
{
	labirint = new int* [nr_linii];
	for (int index = 0; index < nr_linii; index++)
		labirint[index] = new int[nr_coloane];
}

void Harta::citire_harta(string numeFisier)
{
	ifstream file;
	file.open(numeFisier);

	file >> nr_linii >> nr_coloane;

	alocare(nr_linii, nr_coloane);

	for (int indexL = 0; indexL < nr_linii; indexL++)
		for (int indexC = 0; indexC < nr_coloane; indexC++)
			file >> labirint[indexL][indexC];

	file.close();
}

void Harta::afisare_harta()
{
	char patr = 219;

	for (int indexL = 0; indexL < nr_linii; indexL++)
	{
		cout << indexL << " ";
		if (indexL <= 9)
			cout << " ";

		for (int indexC = 0; indexC < nr_coloane; indexC++)
			if (labirint[indexL][indexC] == -1)
				cout << patr;
			else
				if (labirint[indexL][indexC] == -2)
					cout << "*";
				else
					cout << " ";
		cout << endl;
	}
}

void Harta::dealoc()
{
	for (int index = 0; index < nr_linii; index++)
		delete[] labirint[index];
	delete[] labirint;
}

int** Harta::getLabirint()
{
	return labirint;
}

int Harta::getNrLinii()
{
	return nr_linii;
}

int Harta::getNrColoane()
{
	return nr_coloane;
}
