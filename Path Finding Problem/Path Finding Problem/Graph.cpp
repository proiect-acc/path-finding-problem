#include "Graph.h"
#include "Harta.h"
#include <stack>

using namespace std;

int Graph::find_nod(pair<int, int> nod)
{
	for (int index = 0; index < noduri.size(); index++)
		if (nod == noduri[index])
			return index;
	return -1;
}

bool Graph::formare(Harta matrice)
{
	vector<pair<int, int>> temp;

	for (int indexL = 0; indexL < matrice.getNrLinii(); indexL++)
		for (int indexC = 0; indexC < matrice.getNrColoane(); indexC++)
		{
			if (matrice.getLabirint()[indexL][indexC] == 0)
			{
				temp.push_back(make_pair(indexL, indexC));

				lista.push_back(temp);
				noduri.push_back(make_pair(indexL, indexC));

				temp.clear();

				if (indexL != 0 && matrice.getLabirint()[indexL - 1][indexC] == 0)
					lista[find_nod(make_pair(indexL, indexC))].push_back(make_pair(indexL - 1, indexC));

				if (indexL != matrice.getNrLinii() - 1 && matrice.getLabirint()[indexL + 1][indexC] == 0)
					lista[find_nod(make_pair(indexL, indexC))].push_back(make_pair(indexL + 1, indexC));

				if (indexC != 0 && matrice.getLabirint()[indexL][indexC - 1] == 0)
					lista[find_nod(make_pair(indexL, indexC))].push_back(make_pair(indexL, indexC - 1));

				if (indexC != matrice.getNrColoane() && matrice.getLabirint()[indexL][indexC + 1] == 0)
					lista[find_nod(make_pair(indexL, indexC))].push_back(make_pair(indexL, indexC + 1));
			}
		}
	return true;
}

void Graph::afisare_graf()
{
	cout << endl;
	for (int indexNod = 0; indexNod < noduri.size(); indexNod++)
	{
		for (int indexVecin = 0; indexVecin < lista[indexNod].size(); indexVecin++)
			cout << "(" << lista[indexNod][indexVecin].first << ", " << lista[indexNod][indexVecin].second << ") -> ";

		cout << "\b\b" << " " << "\b\b" << " " << "\b\b" << " ";
		cout << endl;
	}
}


stack<int> Graph::aStar(pair<int, int> start, pair<int, int> end)
{
	priority_queue<pair<int, int>, vector<pair<int, int>>, greater <pair<int, int>>> coadaP;

	coadaP.push(start);

	int* precedent;
	int* pretG;
	int* pretF;

	precedent = new int[noduri.size()];

	pretG = new int[noduri.size()];
	for (int index = 0; index < noduri.size(); index++)
		pretG[index] = INT_MAX;

	pretG[find_nod(start)] = 0;

	pretF = new int[noduri.size()];
	for (int index = 0; index < noduri.size(); index++)
		pretF[index] = INT_MAX;

	pretF[find_nod(start)] = cityBlockDistance(start, end);

	while (!coadaP.empty())
	{
		pair<int, int> curent = coadaP.top();

		if (curent == end)
		{
			delete[] pretG;
			delete[] pretF;

			return reconstituire(precedent, start, curent);
		}
		coadaP.pop();

		pair<int, int> next;
		int pretG_test;

		for (int index = 1; index < lista[find_nod(curent)].size(); index++)
		{
			next = lista[find_nod(curent)][index];
			pretG_test = pretG[find_nod(curent)] + 1;

			if (pretG_test < pretG[find_nod(next)])
			{
				precedent[find_nod(next)] = find_nod(curent);
				pretG[find_nod(next)] = pretG_test;
				pretF[find_nod(next)] = pretG[find_nod(next)] + cityBlockDistance(next, end);

				if (!exista(next, coadaP))
					coadaP.push(next);
			}
		}
	}

	delete[] pretG;
	delete[] pretF;
	delete[] precedent;

	cerr << "Nu se poate ajunge la destinatie." << endl;
	exit(0);
}


int Graph::cityBlockDistance(pair<int, int> coord1, pair<int, int> coord2)
{
	return (abs(coord1.first - coord2.first) + abs(coord1.second - coord2.second));
}

stack<int> Graph::reconstituire(int* precedent, pair<int, int> start, pair<int, int> curent)
{
	stack<int> stiva;
	int indice_curent = find_nod(curent);

	while (indice_curent != find_nod(start))
	{
		stiva.push(indice_curent);
		indice_curent = precedent[indice_curent];
	}
	stiva.push(indice_curent);

	delete[] precedent;

	return stiva;
}

bool Graph::exista(pair<int, int> vecin, priority_queue<pair<int, int>, vector<pair<int, int>>, greater <pair<int, int>>> coada)
{
	while (!coada.empty())
	{
		if (coada.top() == vecin)
			return true;
		coada.pop();
	}
	return false;
}

void Graph::afisare_drum(pair<int, int> start, pair<int, int> end, Harta matrice)
{
	vector<pair<stack<int>, int>> solutii;
	stack<int> drum_final;
	int k = 0;

	stack<int> drum_initial = aStar(start, end);
	solutii.push_back(make_pair(drum_initial, drum_initial.size()));

	while (k != 10)
	{
		k++;
		stack<int> drum = aStar(start, end);

		if (drum.size() < solutii[0].second)
		{
			solutii.clear();
			solutii.push_back(make_pair(drum, drum.size()));
			k = 0;
		}
		else
			if (drum.size() == solutii[0].second)
			{
				solutii.push_back(make_pair(drum, drum.size()));
			}
	}

	drum_final = solutii[0].first;

	cout << "Drumul de la (" << start.first << ", " 
		 << start.second << ") la (" << end.first << ", " 
		 << end.second << ") este marcat cu *" << endl 
		 << endl;

	while (!drum_final.empty())
	{
		matrice.labirint[noduri[drum_final.top()].first][noduri[drum_final.top()].second] = -2;
		drum_final.pop();
	}

	matrice.afisare_harta();

	cout << "\b\b " << "\b\b ";
	cout << endl;

}