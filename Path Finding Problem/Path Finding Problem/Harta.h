#pragma once
#include <iostream>
#include <fstream>

using namespace std;

class Harta
{
private:
	int nr_linii;
	int nr_coloane;

public:
	int** labirint;
	void alocare(int, int);
	void citire_harta(string);
	void afisare_harta();
	void dealoc();
	int** getLabirint();
	int getNrLinii();
	int getNrColoane();
};

