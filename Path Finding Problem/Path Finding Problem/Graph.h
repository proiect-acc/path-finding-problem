#pragma once
#include <iostream>
#include <vector>
#include <queue>
#include <stack>
#include "Harta.h"

using namespace std;

class Graph
{
private:
	vector<vector<pair<int, int>>> lista;
	vector<pair<int, int>> noduri;

public:
	int find_nod(pair<int, int>);
	bool formare(Harta);
	void afisare_graf();
	stack<int> aStar(pair<int, int>, pair<int, int>);
	int cityBlockDistance(pair<int, int>, pair<int, int>);
	stack<int> reconstituire(int*, pair<int, int>, pair<int, int>);
	bool exista(pair<int, int>, priority_queue<pair<int, int>, vector<pair<int, int>>, greater <pair<int, int>>>);
	void afisare_drum(pair<int, int>, pair<int, int>, Harta);
};

